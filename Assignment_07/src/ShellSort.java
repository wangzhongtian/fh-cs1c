import java.util.*;
import java.text.*;

import cs_1c.*;

public class ShellSort
{
   private static int Sedgewick_counter = 1; // used for Sedgewick sequence
   private static int s_odd_counter = 0;
   private static int s_even_counter = 0;

   public static <E extends Comparable<? super E>> void shellSort2(E[] a,
         int[] gap_array)
   {
      int k, pos, array_pos, array_size, gap;
      E tmp;
      array_size = a.length;
      array_pos = gap_array.length - 1;

      // uses gap_array value
      for (gap = gap_array[array_pos]; array_pos >= 0; gap = gap_array[--array_pos])
      {
         for (pos = gap; pos < array_size; pos++)
         {
            tmp = a[pos];
            for (k = pos; k >= gap && tmp.compareTo(a[k - gap]) < 0; k -= gap)
               a[k] = a[k - gap];
            a[k] = tmp;
         }

         // to get rid of nullPointerException
         if (array_pos == 0)
            break;
      }
   }

   public static void main(String[] args)
   {
      final int ARRAY_RUN_REP = 3;
      int random_int;
      int added_value = 1; // for Sedgewick
      int tian_added_value = 1; // for Tian

      FHarrayList<Integer> s_array = new FHarrayList<Integer>();// for Sedgewick
      FHarrayList<Integer> t_array = new FHarrayList<Integer>();// for Tian

      NumberFormat tidy = NumberFormat.getInstance(Locale.US);
      tidy.setMaximumFractionDigits(4);
      long start_time, end_time;

      // ==========Creates 6 different ARRAY_SIZE==========
      int[] ARRAY_SIZE = new int[6];
      ARRAY_SIZE[0] = 10000;
      ARRAY_SIZE[1] = 12000;
      ARRAY_SIZE[2] = 14000;
      ARRAY_SIZE[3] = 16000;
      ARRAY_SIZE[4] = 18000;
      ARRAY_SIZE[5] = 20000;

      // ==========Runs the rest of the program with certain ARRAY_SIZE
      for (int sz = 0; sz < 6; sz++)
      {
         System.out.println("\nUsing Array Size: " + sz);

         // ==========Creates lists of arrays of integers==========
         Integer[] array_of_ints; // initiate an array of integers with
                                  // ARRAY_SIZE
         // initiate a list of arrays of integers
         FHarrayList<Integer[]> list_of_int_arr = new FHarrayList<Integer[]>();
         for (int i = 0; i < ARRAY_RUN_REP; i++)
         {
            array_of_ints = new Integer[ARRAY_SIZE[sz]];
            for (int k = 0; k < ARRAY_SIZE[sz]; k++)
            {
               random_int = (int) (Math.random() * ARRAY_SIZE[sz]);
               array_of_ints[k] = random_int;
            }
            list_of_int_arr.add(array_of_ints);
         }

         // use shellSort1
         for (int i = 0; i < ARRAY_RUN_REP; i++)
         {
            System.out.print("shellSort1 Run #" + (i + 1) + ": ");
            start_time = System.nanoTime(); // ------------------ start
            FHsort.shellSort1(list_of_int_arr.get(i));
            end_time = System.nanoTime(); // ---------------------- stop
            System.out.println("Elapsed Time: "
                  + tidy.format((end_time - start_time) / 1e9) + " seconds.");
         }
         // use shellSort2
         int gap_array[] =
         { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192,
               16384, 32768, 65536, 131072, 262144, 524288, 1048576 };

         for (int i = 0; i < ARRAY_RUN_REP; i++)
         {
            System.out.print("shellSort2 Run #" + (i + 1) + ": ");
            start_time = System.nanoTime();
            shellSort2(list_of_int_arr.get(i), gap_array);
            end_time = System.nanoTime();
            System.out.println("Elapsed Time: "
                  + tidy.format((end_time - start_time) / 1e9) + " seconds.");
         }

         // use Sedgewick's gap sequence
         int s_pos;

         for (s_pos = 0; added_value < ARRAY_SIZE[sz]; s_pos++)
         {
            int added_value_temp = added_value;
            added_value = s_add(s_pos);
            if (added_value > ARRAY_SIZE[sz])
            {
               if (Sedgewick_counter % 2 == 0)
               {
                  Sedgewick_counter--;
                  s_odd_counter--;
               } else
               {
                  Sedgewick_counter--;
                  s_even_counter--;
               }
               added_value = added_value_temp;
               break;
            }
            s_array.add(added_value);
         }

         int final_s_array[] = new int[s_array.size()]; // gap array for sort

         for (int i = 0; i < s_array.size(); i++)
         {
            final_s_array[i] = s_array.get(i); // initialize this gap array
         }

         for (int i = 0; i < ARRAY_RUN_REP; i++)
         {
            System.out.print("Sedgewick's Sort Run #" + (i + 1) + ": ");
            start_time = System.nanoTime();
            shellSort2(list_of_int_arr.get(i), final_s_array);
            end_time = System.nanoTime();
            System.out.println("Elapsed Time: "
                  + tidy.format((end_time - start_time) / 1e9) + " seconds.");
         }

         // use Tian's gap sequence
         int t_pos;
         for (t_pos = 0; tian_added_value < ARRAY_SIZE[sz]; t_pos++)
         {
            int added_value_temp = tian_added_value;
            tian_added_value = tian_add(t_pos);
            if (tian_added_value > ARRAY_SIZE[sz])
            {
               tian_added_value = added_value_temp;
               break;
            }
            t_array.add(tian_added_value);
         }
         int final_t_array[] = new int[t_array.size()]; // gap array for sort

         for (int i = 0; i < t_array.size(); i++)
         {
            final_t_array[i] = t_array.get(i); // initialize this gap array
         }

         for (int i = 0; i < ARRAY_RUN_REP; i++)
         {
            System.out.print("Tian's Sort Run #" + (i + 1) + ": ");
            start_time = System.nanoTime();
            shellSort2(list_of_int_arr.get(i), final_t_array);
            end_time = System.nanoTime();
            System.out.println("Elapsed Time: "
                  + tidy.format((end_time - start_time) / 1e9) + " seconds.");
         }
      }
   }

   private static int s_add(int s_pos)
   {
      if (Sedgewick_counter % 2 != 0)
      {
         int a = (int) Math.pow(4, s_odd_counter);
         int b = (int) Math.pow(2, s_odd_counter);
         Sedgewick_counter++;
         s_odd_counter++;
         return 9 * (a - b) + 1;
      } else
      {
         int c = (int) Math.pow(2, s_even_counter + 2);
         Sedgewick_counter++;
         s_even_counter++;
         return c * (c - 3) + 1;
      }
   }

   private static int tian_add(int t_pos)
   {
      return (int) Math.pow(t_pos, t_pos);
   }
}

/* ==================================OUTPUT====================================

Using Array Size: 0
shellSort1 Run #1: Elapsed Time: 0.0369 seconds.
shellSort1 Run #2: Elapsed Time: 0.0267 seconds.
shellSort1 Run #3: Elapsed Time: 0.023 seconds.
shellSort2 Run #1: Elapsed Time: 0.0146 seconds.
shellSort2 Run #2: Elapsed Time: 0.0109 seconds.
shellSort2 Run #3: Elapsed Time: 0.0134 seconds.
Sedgewick Sort Run #1: Elapsed Time: 0.0098 seconds.
Sedgewick Sort Run #2: Elapsed Time: 0.0104 seconds.
Sedgewick Sort Run #3: Elapsed Time: 0.0087 seconds.
Tian's Sort Run #1: Elapsed Time: 0.0055 seconds.
Tian's Sort Run #2: Elapsed Time: 0.0079 seconds.
Tian's Sort Run #3: Elapsed Time: 0.0007 seconds.

Using Array Size: 1
shellSort1 Run #1: Elapsed Time: 0.0056 seconds.
shellSort1 Run #2: Elapsed Time: 0.005 seconds.
shellSort1 Run #3: Elapsed Time: 0.0035 seconds.
shellSort2 Run #1: Elapsed Time: 0.0014 seconds.
shellSort2 Run #2: Elapsed Time: 0.0014 seconds.
shellSort2 Run #3: Elapsed Time: 0.0014 seconds.
Sedgewick Sort Run #1: Elapsed Time: 0.0012 seconds.
Sedgewick Sort Run #2: Elapsed Time: 0.0011 seconds.
Sedgewick Sort Run #3: Elapsed Time: 0.0011 seconds.
Tian's Sort Run #1: Elapsed Time: 0.0013 seconds.
Tian's Sort Run #2: Elapsed Time: 0.0013 seconds.
Tian's Sort Run #3: Elapsed Time: 0.0014 seconds.

Using Array Size: 2
shellSort1 Run #1: Elapsed Time: 0.0035 seconds.
shellSort1 Run #2: Elapsed Time: 0.004 seconds.
shellSort1 Run #3: Elapsed Time: 0.0037 seconds.
shellSort2 Run #1: Elapsed Time: 0.0018 seconds.
shellSort2 Run #2: Elapsed Time: 0.0021 seconds.
shellSort2 Run #3: Elapsed Time: 0.0031 seconds.
Sedgewick Sort Run #1: Elapsed Time: 0.0016 seconds.
Sedgewick Sort Run #2: Elapsed Time: 0.0013 seconds.
Sedgewick Sort Run #3: Elapsed Time: 0.0014 seconds.
Tian's Sort Run #1: Elapsed Time: 0.003 seconds.
Tian's Sort Run #2: Elapsed Time: 0.0027 seconds.
Tian's Sort Run #3: Elapsed Time: 0.0028 seconds.

Using Array Size: 3
shellSort1 Run #1: Elapsed Time: 0.0071 seconds.
shellSort1 Run #2: Elapsed Time: 0.0052 seconds.
shellSort1 Run #3: Elapsed Time: 0.0054 seconds.
shellSort2 Run #1: Elapsed Time: 0.0028 seconds.
shellSort2 Run #2: Elapsed Time: 0.0021 seconds.
shellSort2 Run #3: Elapsed Time: 0.0032 seconds.
Sedgewick Sort Run #1: Elapsed Time: 0.0018 seconds.
Sedgewick Sort Run #2: Elapsed Time: 0.0017 seconds.
Sedgewick Sort Run #3: Elapsed Time: 0.0018 seconds.
Tian's Sort Run #1: Elapsed Time: 0.0057 seconds.
Tian's Sort Run #2: Elapsed Time: 0.0062 seconds.
Tian's Sort Run #3: Elapsed Time: 0.0065 seconds.

Using Array Size: 4
shellSort1 Run #1: Elapsed Time: 0.0074 seconds.
shellSort1 Run #2: Elapsed Time: 0.0062 seconds.
shellSort1 Run #3: Elapsed Time: 0.0067 seconds.
shellSort2 Run #1: Elapsed Time: 0.0033 seconds.
shellSort2 Run #2: Elapsed Time: 0.0025 seconds.
shellSort2 Run #3: Elapsed Time: 0.0044 seconds.
Sedgewick Sort Run #1: Elapsed Time: 0.0033 seconds.
Sedgewick Sort Run #2: Elapsed Time: 0.0043 seconds.
Sedgewick Sort Run #3: Elapsed Time: 0.004 seconds.
Tian's Sort Run #1: Elapsed Time: 0.0105 seconds.
Tian's Sort Run #2: Elapsed Time: 0.0087 seconds.
Tian's Sort Run #3: Elapsed Time: 0.0066 seconds.

Using Array Size: 5
shellSort1 Run #1: Elapsed Time: 0.007 seconds.
shellSort1 Run #2: Elapsed Time: 0.0078 seconds.
shellSort1 Run #3: Elapsed Time: 0.0073 seconds.
shellSort2 Run #1: Elapsed Time: 0.0046 seconds.
shellSort2 Run #2: Elapsed Time: 0.0048 seconds.
shellSort2 Run #3: Elapsed Time: 0.0043 seconds.
Sedgewick Sort Run #1: Elapsed Time: 0.0027 seconds.
Sedgewick Sort Run #2: Elapsed Time: 0.0037 seconds.
Sedgewick Sort Run #3: Elapsed Time: 0.004 seconds.
Tian's Sort Run #1: Elapsed Time: 0.0079 seconds.
Tian's Sort Run #2: Elapsed Time: 0.0075 seconds.
Tian's Sort Run #3: Elapsed Time: 0.011 seconds.
*/

/*========================ANSWER TO THE ASSIGNED QUESTION======================
      Q: Why do you think Shell's gap sequence implied by shellSort1() gives a
         different timing result than the explicit array described above and 
         passed to shellSort2()?

      A: For shellSort2, each pair of neighboring gap values in this sequence 
         is relatively prime. Therefore, the time needed to do insertion in the
         left of an element is more quickly. Applying the formula for big Oh, 
         we get that this sequence (Hibbard Sequence) is O(N^(3/2)), while 
         shellSort1 (using quadratic) is O(N^2), shellSort2 is faster than 
         shellSort1.
*/