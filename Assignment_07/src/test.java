public class test
{
   static int s_odd_counter = 0;
   static int s_even_counter = 0;
   static int Sedgewick_counter = 1;
   public static void main(String[] args)
   {
      int result = 0;
      for (int position = 0; position < 15; position++)
      {
         System.out.println(s_add(position));
      }

   }
   
   private static int s_add(int s_pos)
   {
      if (Sedgewick_counter % 2 != 0)
      {
         int a = (int) Math.pow(4, s_odd_counter);
         int b = (int) Math.pow(2, s_odd_counter);
         Sedgewick_counter++;
         s_odd_counter++;
         return 9 * (a - b) + 1;
      }
      else
      {
         int c = (int) Math.pow(2, s_even_counter + 2);
         Sedgewick_counter++;
         s_even_counter++;
         
         return c * (c - 3) + 1;
         
      }
      
   }
}