import cs_1c.*;
import java.util.*;
import java.text.*;

//------------------------------------------------------
public class Foothill
{
   // -------  main --------------
   public static void main(String[] args) throws Exception
   {
      final int ARRAY_SIZE = 50000;
      int k, random_int;
      long start_time, end_time; 
      // for formatting output neatly
      NumberFormat tidy = NumberFormat.getInstance(Locale.US);
      tidy.setMaximumFractionDigits(4);

      Integer[] array_of_ints1 = new Integer[ARRAY_SIZE];
      Integer[] array_of_ints2 = new Integer[ARRAY_SIZE];
      Integer[] array_of_ints3 = new Integer[ARRAY_SIZE];

      // build three arrays for comparing sorts
      for (k = 0; k < ARRAY_SIZE; k++)
      {
         random_int = (int) (Math.random() * ARRAY_SIZE);
         array_of_ints1[k] = random_int;
         array_of_ints2[k] = random_int;
         array_of_ints3[k] = random_int;
      }
      start_time = System.nanoTime();  // ------------------ start 
      FHsort.insertionSort(array_of_ints1);
      end_time = System.nanoTime();    // ---------------------- stop
      System.out.println("Insertion sort Elapsed Time: "
            + tidy.format( (end_time - start_time) / 1e9)
            + " seconds.");

      start_time = System.nanoTime();  // ------------------ start   
      FHsort.shellSort1(array_of_ints2);
      end_time = System.nanoTime();    // ---------------------- stop
      System.out.println("Shell Sort Elapsed Time: "
            + tidy.format( (end_time - start_time) / 1e9)
            + " seconds.");
      
      start_time = System.nanoTime();  // ------------------ start   
      Arrays.sort(array_of_ints3);
      end_time = System.nanoTime();    // ---------------------- stop
      System.out.println("Arrays.sort()  Elapsed Time: "
            + tidy.format( (end_time - start_time) / 1e9)
            + " seconds.");

      /* for (k = 0; k < ARRAY_SIZE; k+= ARRAY_SIZE/10)
      {
         System.out.println( "bubble #" + k + ": " + array_of_ints1[k] + ", ");
         System.out.println( "heap #" + k + ": " + array_of_ints1[k] );
      } */
   }
}
