/**
 * Top-Down Splay Trees Using Inheritance
 * 
 * @author Zhongtian Wang
 */
import cs_1c.*;

public class SplayTree<E extends Comparable<? super E>> extends
      FHsearch_tree<E>
{
   public boolean insert(E x)
   {
      if (m_root == null)
      {
         m_root = new FHs_treeNode<E>(x, null, null);
         m_size++;
         return true;
      } else
      {
         m_root = splay(m_root, x);
      }
      int compare_result;
      compare_result = x.compareTo(m_root.data);
      FHs_treeNode<E> new_root;
      if (compare_result < 0)
      {
         new_root = new FHs_treeNode<E>(x, m_root.l_child, m_root);
         m_root.l_child = null;
         m_root = new_root;
         m_size++;
         return true;
      } else if (compare_result > 0)
      {
         new_root = new FHs_treeNode<E>(x, m_root, m_root.r_child);
         m_root.r_child = null;
         m_root = new_root;
         m_size++;
         return true;
      }
      return false;
   }

   public boolean remove(E x)
   {
      if (m_root == null)
      {
         return false;
      }

      m_root = splay(m_root, x);
      if (x.compareTo(m_root.data) != 0)
         return false;

      if (m_root.l_child == null)
         m_root = m_root.r_child;
      else
      {
         m_root = m_root.l_child;
         m_root = splay(m_root, x);
         m_root.r_child = m_root.r_child;
      }
      return true;
   }

   public E showRoot()
   {
      if (m_root == null)
         return null;
      return m_root.data;
   }

   protected FHs_treeNode<E> splay(FHs_treeNode<E> root, E x)
   {
      FHs_treeNode<E> right_tree = null, left_tree = null, right_tree_min = null, left_tree_max = null;
      while (root != null)
      {
         int compare_result;
         compare_result = x.compareTo(root.data);
         if (compare_result < 0)
         {
            if (root.l_child == null)
            {
               break;
            }
            if (x.compareTo(root.l_child.data) < 0)
            {
               root = rotateWithLeftChild(root);
               if (root.l_child == null)
                  break;
            }
            if (right_tree == null)
            {
               right_tree = root;
               right_tree_min = right_tree;
            } else
            {
               right_tree_min.l_child = root;
               right_tree_min = right_tree_min.l_child;
            }
            root = root.l_child;
            right_tree_min.l_child = null;
         } else if (compare_result > 0)
         {
            if (root.r_child == null)
            {
               break;
            }
            if (x.compareTo(root.r_child.data) > 0)
            {
               root = rotateWithRightChild(root);
               if (root.r_child == null)
                  break;
            }
            if (right_tree == null)
            {
               left_tree = root;
               left_tree_max = left_tree;
            } else
            {
               left_tree_max.r_child = root;
               left_tree_max = left_tree_max.r_child;
            }
            root = root.r_child;
            left_tree_max.r_child = null;
         } else
         {
            break;
         }
      }
      if (left_tree != null)
      {
         left_tree_max.r_child = root.l_child;
         root.l_child = left_tree;
      }
      if (right_tree != null)
      {
         right_tree_min.l_child = root.r_child;
         root.r_child = right_tree;
      }
      return root;
   }

   protected FHs_treeNode<E> rotateWithLeftChild(FHs_treeNode<E> k2)
   {
      FHs_treeNode<E> k1 = k2.l_child;
      k2.l_child = k1.r_child;
      k1.r_child = k2;
      return k1;
   }

   protected FHs_treeNode<E> rotateWithRightChild(FHs_treeNode<E> k2)
   {
      FHs_treeNode<E> k1 = k2.r_child;
      k2.r_child = k1.l_child;
      k1.l_child = k2;
      return k1;
   }

   protected FHs_treeNode<E> find(FHs_treeNode<E> root, E x)
   {
      m_root = splay(root, x);
      if (x.compareTo(m_root.data) == 0)
         return m_root;
      else
         return null;
   }

}


/*========================OUTPUT=====================
Initial size: 0
New size: 32

Traversal
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
 31 32 
 oops 
splay -1 --> root: 1 height: 16
 oops 
splay 0 --> root: 1 height: 16
splay 1 --> root: 1 height: 16
splay 2 --> root: 2 height: 9
splay 3 --> root: 3 height: 6
splay 4 --> root: 4 height: 6
splay 5 --> root: 5 height: 5
splay 6 --> root: 6 height: 6
splay 7 --> root: 7 height: 6
splay 8 --> root: 8 height: 7
splay 9 --> root: 9 height: 8

=========================================================*/