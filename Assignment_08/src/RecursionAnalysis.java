import java.text.NumberFormat;
import java.util.Locale;
import java.io.*;
import cs_1c.FHsort;

public class RecursionAnalysis
{
   public static void main(String[] args)
   {
      int array_size = 20000;
      boolean toContinue = true;
      NumberFormat tidy = NumberFormat.getInstance(Locale.US);
      tidy.setMaximumFractionDigits(4);
      long start_time, end_time;

      PrintStream out = null;
      try
      {
         out = new PrintStream(new FileOutputStream("run.txt"));

         while (toContinue)
         {
            try
            {
               Integer qs_array[] = new Integer[array_size];
               for (int i = 0; i < array_size; i++)
                  qs_array[i] = (int) (-200 + Math.random() * 400);

               for (int i = 1; i <= 150; i++)
               {
                  FHsort.setRecursionLimit(2 * i);

                  start_time = System.nanoTime(); // ------------------ start
                  FHsort.quickSort(qs_array);
                  end_time = System.nanoTime(); // ---------------------- stop
                  out.println("Array Size = " + array_size
                        + ", Recursion Limit = " + (2 * i) + ", Elapsed Time: "
                        + tidy.format((end_time - start_time) / 1e9)
                        + " seconds.");
                  System.out.println("Array Size = " + array_size
                        + ", Recursion Limit = " + (2 * i) + ", Elapsed Time: "
                        + tidy.format((end_time - start_time) / 1e9)
                        + " seconds.");
               }

               if (array_size < 500000)
                  array_size += 5000;
               else if (array_size > 500000 && array_size < 5000000)
                  array_size += 50000;
               else
                  array_size += 500000;
            } catch (OutOfMemoryError e)
            {
               System.out.println("Out of Memory!");
               toContinue = false;
            }
         }
      } catch (Exception e)
      {
         System.out.println("File output error");
      }
   }
}


/*================Sample Run==============
Array Size = 20000, Recursion Limit = 2, Elapsed Time: 0.1029 seconds.
Array Size = 20000, Recursion Limit = 4, Elapsed Time: 0.141 seconds.
Array Size = 20000, Recursion Limit = 6, Elapsed Time: 0.0452 seconds.
Array Size = 20000, Recursion Limit = 8, Elapsed Time: 0.0455 seconds.
Array Size = 20000, Recursion Limit = 10, Elapsed Time: 0.0319 seconds.
Array Size = 20000, Recursion Limit = 12, Elapsed Time: 0.0347 seconds.
Array Size = 20000, Recursion Limit = 14, Elapsed Time: 0.0444 seconds.
Array Size = 20000, Recursion Limit = 16, Elapsed Time: 0.0361 seconds.
Array Size = 20000, Recursion Limit = 18, Elapsed Time: 0.0305 seconds.
Array Size = 20000, Recursion Limit = 20, Elapsed Time: 0.0397 seconds.
Array Size = 20000, Recursion Limit = 22, Elapsed Time: 0.0274 seconds.
Array Size = 20000, Recursion Limit = 24, Elapsed Time: 0.0234 seconds.
Array Size = 20000, Recursion Limit = 26, Elapsed Time: 0.0213 seconds.
Array Size = 20000, Recursion Limit = 28, Elapsed Time: 0.0222 seconds.
Array Size = 20000, Recursion Limit = 30, Elapsed Time: 0.0226 seconds.
Array Size = 20000, Recursion Limit = 32, Elapsed Time: 0.0251 seconds.
Array Size = 20000, Recursion Limit = 34, Elapsed Time: 0.0188 seconds.
Array Size = 20000, Recursion Limit = 36, Elapsed Time: 0.0228 seconds.
Array Size = 20000, Recursion Limit = 38, Elapsed Time: 0.0184 seconds.
Array Size = 20000, Recursion Limit = 40, Elapsed Time: 0.02 seconds.
Array Size = 20000, Recursion Limit = 42, Elapsed Time: 0.0199 seconds.
Array Size = 20000, Recursion Limit = 44, Elapsed Time: 0.0146 seconds.
Array Size = 20000, Recursion Limit = 46, Elapsed Time: 0.012 seconds.
Array Size = 20000, Recursion Limit = 48, Elapsed Time: 0.0111 seconds.
Array Size = 20000, Recursion Limit = 50, Elapsed Time: 0.0108 seconds.
Array Size = 20000, Recursion Limit = 52, Elapsed Time: 0.0101 seconds.
Array Size = 20000, Recursion Limit = 54, Elapsed Time: 0.0089 seconds.
...
Array Size = 20000, Recursion Limit = 290, Elapsed Time: 0.0008 seconds.
Array Size = 20000, Recursion Limit = 292, Elapsed Time: 0.0008 seconds.
Array Size = 20000, Recursion Limit = 294, Elapsed Time: 0.0008 seconds.
Array Size = 20000, Recursion Limit = 296, Elapsed Time: 0.0008 seconds.
Array Size = 20000, Recursion Limit = 298, Elapsed Time: 0.0006 seconds.
Array Size = 20000, Recursion Limit = 300, Elapsed Time: 0.0008 seconds.
Array Size = 25000, Recursion Limit = 2, Elapsed Time: 0.0037 seconds.
Array Size = 25000, Recursion Limit = 4, Elapsed Time: 0.0021 seconds.
Array Size = 25000, Recursion Limit = 6, Elapsed Time: 0.0021 seconds.
Array Size = 25000, Recursion Limit = 8, Elapsed Time: 0.0019 seconds.
Array Size = 25000, Recursion Limit = 10, Elapsed Time: 0.0018 seconds.
Array Size = 25000, Recursion Limit = 12, Elapsed Time: 0.0014 seconds.
Array Size = 25000, Recursion Limit = 14, Elapsed Time: 0.0017 seconds.
Array Size = 25000, Recursion Limit = 16, Elapsed Time: 0.0016 seconds.
Array Size = 25000, Recursion Limit = 18, Elapsed Time: 0.0016 seconds.
Array Size = 25000, Recursion Limit = 20, Elapsed Time: 0.0016 seconds.
Array Size = 25000, Recursion Limit = 22, Elapsed Time: 0.0016 seconds.
Array Size = 25000, Recursion Limit = 24, Elapsed Time: 0.0016 seconds.
Array Size = 25000, Recursion Limit = 26, Elapsed Time: 0.0016 seconds.
Array Size = 25000, Recursion Limit = 28, Elapsed Time: 0.0016 seconds.
Array Size = 25000, Recursion Limit = 30, Elapsed Time: 0.0016 seconds.
Array Size = 25000, Recursion Limit = 32, Elapsed Time: 0.0016 seconds.
Array Size = 25000, Recursion Limit = 34, Elapsed Time: 0.0015 seconds.
Array Size = 25000, Recursion Limit = 36, Elapsed Time: 0.0015 seconds.
Array Size = 25000, Recursion Limit = 38, Elapsed Time: 0.0015 seconds.
...
Many Entries Below.... but this is the idea: for each array size, set recursion
limit from 2 to 300, run array size bigger and bigger until out of memory.
 
 Exporting all the data entries into an excel file, shows that starting at 
 array size = 3000000, the elapsed time becomes not flat anymore. So for array
 size bigger than 20000 and smaller than 3000000, the elapsed time seems flat,
 and it grows relatively at constant acceleration.
 
 The detailed result is showed in the excel file uploaded with this java file.
*/