// file LazySearchTree.java
import java.util.*;
import cs_1c.*;

/**
 * Implements Lazy Binary Search Tree
 * 
 * @author Zhongtian Wang
 * @version 05/08/2013
 */
public class LazySearchTree<E extends Comparable<? super E>> implements
      Cloneable
{
   protected int m_size;
   protected LazySTNode<E> m_root;
   protected int m_size_hard;

   // testing purpose
   int sizeHard()
   {
      return m_size_hard;
   }

   public LazySearchTree()
   {
      clear();
   }

   public boolean empty()
   {
      return (m_size == 0);
   }

   public int size()
   {
      return m_size;
   }

   public void clear()
   {
      m_size = 0;
      m_root = null;
   }

   public int showHeight()
   {
      return findHeight(m_root, -1);
   }

   public E findMin()
   {
      if (m_root == null)
         throw new NoSuchElementException();
      return findMin(m_root).data;
   }

   public E findMax()
   {
      if (m_root == null)
      {
         throw new NoSuchElementException();
      }
      return findMax(m_root).data;
   }

   public E find(E x)
   {
      LazySTNode<E> result_node;
      result_node = find(m_root, x);
      if (result_node == null)
         throw new NoSuchElementException();
      return result_node.data;
   }

   public boolean contains(E x)
   {
      return find(m_root, x) != null;
   }

   public boolean insert(E x)
   {
      int old_size = m_size;
      m_root = insert(m_root, x);
      return (m_size != old_size);
   }

   public boolean remove(E x)
   {
      int old_size = m_size;
      remove(m_root, x);
      return (m_size != old_size);
   }

   public <F extends Traverser<? super E>> void traverse(F func)
   {
      traverse(func, m_root);
   }

   public Object clone() throws CloneNotSupportedException
   {
      LazySearchTree<E> new_object = (LazySearchTree<E>) super.clone();
      new_object.clear(); // can't point to other's data

      new_object.m_root = cloneSubtree(m_root);
      new_object.m_size = m_size;
      // need to traverse through the tree and clone the hard deleted ref!!!!!!

      return new_object;
   }

   public void collectGarbage()
   {
      collectGarbage(m_root);
   }

   // private helper methods ----------------------------------------
   protected LazySTNode<E> findMin(LazySTNode<E> root)
   {

      if (root.deleted && root.l_child != null)
         return findMin(root.l_child);
      if (root.deleted && root.l_child == null)
         return null;
      if (!root.deleted && root.l_child != null)
         return findMin(root.l_child);
      if (!root.deleted && root.l_child == null)
         return null;
      return null; // re-CHECK!!!!!
   }

   protected LazySTNode<E> findMinHard(LazySTNode<E> root)
   {
      if (root == null)
         return null;
      if (root.l_child == null)
         return root;
      return findMinHard(root.l_child);
   }

   protected LazySTNode<E> findMax(LazySTNode<E> root)
   {
      System.out.println("\nroot.data=" + root.data);
      if (root.deleted && root.r_child != null)
         return findMax(root.r_child);
      if (root.deleted && root.r_child == null)
         return null;
      if (!root.deleted && root.r_child != null)
         return findMax(root.r_child);
      if (!root.deleted && root.r_child == null)
         return root;
      return null;
   }

   protected LazySTNode<E> findMaxHard(LazySTNode<E> root)
   {
      if (root == null)
         return null;
      if (root.r_child == null)
         return root;
      return findMinHard(root.r_child);
   }

   protected LazySTNode<E> insert(LazySTNode<E> root, E x)
   {
      int compare_result; // avoid multiple calls to compareTo()

      if (root == null) // root == null
      {
         m_size++;
         m_size_hard++;
         return new LazySTNode<E>(x, null, null);
      }
      if (root.deleted == true) // root soft deleted
      {
         m_size++;
         if (root.data == x) // root soft deleted && data equals
         {
            root.deleted = false;
         } else
         // root soft deleted && data does not equal
         {
            compare_result = x.compareTo(root.data);
            if (compare_result < 0)
               root.l_child = insert(root.l_child, x);
            else if (compare_result > 0)
               root.r_child = insert(root.r_child, x);
         }
      } else
      // root exists
      {
         compare_result = x.compareTo(root.data);
         if (compare_result < 0)
            root.l_child = insert(root.l_child, x);
         else if (compare_result > 0)
            root.r_child = insert(root.r_child, x);
      }
      return root;
   }

   protected void remove(LazySTNode<E> root, E x)
   {
      if (root != null)
      {
         int compare_result;
         compare_result = x.compareTo(root.data);
         if (compare_result < 0)
            remove(root.l_child, x);
         if (compare_result > 0)
            remove(root.r_child, x);
         if (compare_result == 0 && !root.deleted)
         {
            root.deleted = true;
            m_size--;
         }
      }
   }

   protected void removeHard(LazySTNode<E> root, E x)
   {
      System.out.println("removeHard: " + x);
      if (root != null)
      {
         int compare_result;

         compare_result = x.compareTo(root.data);
         if (compare_result < 0)
            removeHard(root.l_child, x);
         if (compare_result > 0)
            removeHard(root.r_child, x);
         if (compare_result == 0)
         {
            if (root.l_child != null && root.r_child != null)
            {
               root.data = findMinHard(root.r_child).data;
               root.deleted = false;

               System.out.println("!!root.r_child=" + root.r_child);
               removeHard(root.r_child, root.data);
               //root.r_child = null;
               System.out.println("!!after hard remove... root.r_child="
                     + root.r_child + "\nits data= " + root.r_child.data);
            } 
            else
            {
               System.out.println("deleting root loc " + root);
               if (root.r_child == null)
               {
                  root = root.l_child;
                  root.data = root.l_child.data;
                  root.l_child = root.l_child.l_child;
                  root.r_child = root.l_child.r_child;
               }
               if (root.l_child == null)
               {
                  root = root.r_child;
                  root.data = root.r_child.data;
                  root.l_child = root.r_child.l_child;
                  root.r_child = root.r_child.r_child;
               }
               System.out.println(root == null);
               System.out.println("memo loc of root " + root);

               m_size_hard--;
            }
         }
      }
   }

   protected void collectGarbage(LazySTNode<E> root)
   {
      if (root != null)
      {
         collectGarbage(root.l_child);
         collectGarbage(root.r_child);
         if (root.deleted)
            removeHard(root, root.data);
      }
   }

   protected <F extends Traverser<? super E>> void traverse(F func,
         LazySTNode<E> tree_node)
   {
      if (tree_node == null)
         return;

      // System.out.println("LLL");
      traverse(func, tree_node.l_child);
      // System.out.println("NNN");
      func.visit(tree_node.data);
      // System.out.println("RRR");
      traverse(func, tree_node.r_child);
   }

   protected LazySTNode<E> find(LazySTNode<E> root, E x)
   {
      int compare_result; // avoid multiple calls to compareTo()

      if (root == null || root.deleted)
         return null;

      compare_result = x.compareTo(root.data);
      if (compare_result < 0)
         return find(root.l_child, x);
      if (compare_result > 0)
         return find(root.r_child, x);
      return root; // found
   }

   protected LazySTNode<E> cloneSubtree(LazySTNode<E> root)
   {
      LazySTNode<E> new_node;
      if (root == null)
         return null;

      // does not set my_root which must be done by caller
      new_node = new LazySTNode<E>(root.data, cloneSubtree(root.l_child),
            cloneSubtree(root.r_child));
      return new_node;
   }

   protected int findHeight(LazySTNode<E> tree_node, int height)
   {
      int left_height, right_height;
      if (tree_node == null)
         return height;
      height++;
      left_height = findHeight(tree_node.l_child, height);
      right_height = findHeight(tree_node.r_child, height);
      return (left_height > right_height) ? left_height : right_height;
   }
}

class LazySTNode<E extends Comparable<? super E>>
{
   boolean deleted = false;
   public LazySTNode<E> l_child, r_child;
   public E data;
   public LazySTNode<E> my_root; // needed to test for certain error

   public LazySTNode(E d, LazySTNode<E> lt, LazySTNode<E> rt)
   {
      l_child = lt;
      r_child = rt;
      data = d;
   }

   public LazySTNode()
   {
      this(null, null, null);
   }

   // function stubs -- for use only with AVL Trees when we extend
   public int getHeight()
   {
      return 0;
   }

   boolean setHeight(int height)
   {
      return true;
   }
}
