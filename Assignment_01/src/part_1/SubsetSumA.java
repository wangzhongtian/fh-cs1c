package part_1;

import java.text.NumberFormat;
import java.util.*;

/**
 * Test class SubsetSumA to test class Sublist
 * 
 * @author Zhongtian Wang
 * @version April 17, 2013
 */
public class SubsetSumA
{
   public static void main(String[] args) throws Exception
   {
      double TARGET = 47;
      ArrayList<Double> data_set = new ArrayList<Double>();
      ArrayList<Sublist> choices = new ArrayList<Sublist>();
      Sublist empty = new Sublist(data_set);
      int i, j, max_index, current_size;
      double max_temp = 0, max_sum = 0;
      boolean found_perfect = false;

      data_set.add(2.2);
      data_set.add(12.5);
      data_set.add(22.7);
      data_set.add(5.1);
      data_set.add(15.4);
      data_set.add(25.0);
      data_set.add(9.9);
      data_set.add(19.7);
      data_set.add(29.6);

      NumberFormat tidy = NumberFormat.getInstance(Locale.US);
      tidy.setMaximumFractionDigits(4);

      long start_time, end_time;
      start_time = System.nanoTime();

      max_index = 0;
      choices.add(empty);

      for (i = 0; i < data_set.size(); i++)
      {
         current_size = choices.size();
         for (j = 0; j < current_size; j++)
         {
            double sum = choices.get(j).getSum() + data_set.get(i);
            if (sum < TARGET)
            {
               choices.add(choices.get(j).addItem(i));
            } else if (sum == TARGET)
            {
               choices.add(choices.get(j).addItem(i));
               found_perfect = true;
               max_sum = sum;
               max_index = choices.size() - 1;
               break;
            }
         }
         if (found_perfect)
         {
            break;
         }
      }

      if (!found_perfect)
      {
         for (int k = 0; k < choices.size(); k++)
         {
            max_sum = choices.get(k).getSum();
            if (max_sum >= max_temp)
            {
               max_temp = max_sum;
               max_index = k;
            }
         }
      }

      end_time = System.nanoTime();
      System.out.println("Target time: " + TARGET
            + "\n-------------------------------------------------"
            + "\nAlgorithms Elapsed Time: "
            + tidy.format((end_time - start_time) / 1e9) + " seconds."
            + "\n-------------------------------------------------");
      choices.get(max_index).showSublist();
   }

   /**
    * Class Sublist
    * 
    * @author Zhongtian Wang
    * @version April 17, 2013
    */
   private static class Sublist implements Cloneable
   {
      private double sum = 0;
      private ArrayList<Double> original_objects;
      private ArrayList<Integer> indices;

      /**
       * Default constructor that constructs an empty Subset object
       */
      public Sublist(ArrayList<Double> orig)
      {
         sum = 0;
         original_objects = orig;
         indices = new ArrayList<Integer>();
      }

      /**
       * Returns sum
       * 
       * @return sum
       */
      double getSum()
      {
         return sum;
      }

      /**
       * Clones this Sublist object
       * 
       * @throws CloneNotSupportedException
       * @return the cloned object "new_object"
       */
      public Object clone() throws CloneNotSupportedException
      {
         Sublist new_object = (Sublist) super.clone();
         new_object.indices = (ArrayList<Integer>) indices.clone();

         return new_object;
      }

      /**
       * Returns a new Sublist object with one reference added in the end
       * 
       * @param index
       * @return newList the new Sublist object
       */
      Sublist addItem(int index)
      {
         Sublist newList = new Sublist(original_objects);
         try
         {
            newList = (Sublist) this.clone();
         } catch (CloneNotSupportedException e)
         {
            System.out.println(e);
         }
         newList.sum += original_objects.get(index);
         newList.indices.add(index);
         return newList;
      }

      /**
       * Prints the Sublist object to the console
       */
      void showSublist()
      {
         System.out.println("Sum = " + sum);
         for (int i = 0; i < indices.size(); i++)
            System.out.println("array[" + indices.get(i) + "] = "
                  + original_objects.get(indices.get(i)));
      }
   }
}

/*-------------------OUTPUT--------------------

 Target time: 47.0
 -------------------------------------------------
 Algorithms Elapsed Time: 0.0012 seconds.
 -------------------------------------------------
 Sum = 46.9
 array[0] = 2.2
 array[5] = 25.0
 array[7] = 19.7

 -----------------------------------------------*/