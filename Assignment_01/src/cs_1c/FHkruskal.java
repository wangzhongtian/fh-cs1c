package cs_1c;
import java.util.*;

public class FHkruskal<E>
{
   private PriorityQueue< FHedge<E> > edge_heap;
   FHgraph<E> in_graph;

   public FHkruskal (FHgraph<E> grph)
   {
      this();
      setInGraph(grph); 
   }

   public FHkruskal ()
   {
      edge_heap = new PriorityQueue< FHedge<E> >();
      in_graph = null;
   }

   public void clear()
   {
      edge_heap.clear();
   }

   public void setInGraph(FHgraph<E> grph)
   { 
      in_graph = grph;
      clear();
   }

   // algorithms
   public FHgraph<E> genKruskal()
   {
      Iterator< FHvertex<E> > iter;
      LinkedList< HashSet<FHvertex<E>> > vertex_sets
          = new LinkedList< HashSet<FHvertex<E>> >();
      Iterator< HashSet<FHvertex<E>> > f_iter;
      HashSet<FHvertex<E>> verts_in_graph, singleton, vert_set,
         vert_set_src = null, vert_set_dst = null;
      FHedge<E> smallest_edge;
      FHvertex<E> src, dst, vert;
      ArrayList< FHedge<E> > new_edges = new ArrayList< FHedge<E> >();
      int k, num_verts_found;
      FHgraph<E> out_graph = new FHgraph<E>();

      if (in_graph == null)
         return null;

      // get a local list of vertices
      verts_in_graph = in_graph.getVertSet();

      // form a forest of sets, initializing each with only 
      // one vertex from the graph
      for (k = 0, iter = verts_in_graph.iterator(); 
         iter.hasNext(); k++)
      {
         vert = iter.next(); 
         singleton = new HashSet<FHvertex<E>>();
         singleton.add(vert);
         vertex_sets.add( singleton );
      }

      // form a binary min heap of edges so we can easily find min costs
      if (!buildEdgeHeap())
         return null;

      // test for empty to avoid inf. loop resulting from disconnected graph
      while (!edge_heap.isEmpty() && vertex_sets.size() > 1)
      {
         // pop smallest edge left in heap
         smallest_edge = edge_heap.remove();
         src = smallest_edge.source;
         dst = smallest_edge.dest;

         // see if src and dst are in different sets.  if so, take union
         for (f_iter = vertex_sets.iterator(), num_verts_found = 0 ; 
            f_iter.hasNext()  &&  (num_verts_found < 2) ; )
         {
            vert_set = f_iter.next();
            if ( vert_set.contains(src) )
            {
               vert_set_src = vert_set;
               num_verts_found++;
            }

            if ( vert_set.contains(dst) )
            {
               vert_set_dst = vert_set;
               num_verts_found++;
            }
         }
         if (vert_set_src == vert_set_dst)  // same sets: reject
            continue;

         new_edges.add(smallest_edge);
         vert_set_src.addAll(vert_set_dst);
         vertex_sets.remove(vert_set_dst);
      }
      
      out_graph.setGraph(new_edges);
      return out_graph;
   }

   private boolean buildEdgeHeap()
   {
      HashSet< FHvertex<E> > verts_in_graph;
      Iterator< FHvertex<E> > vert_iter;
      Iterator< Pair<FHvertex<E>, Double> > edge_iter;
      FHvertex<E> src, dst;
      Pair<FHvertex<E>, Double> edge;
      double cost;
      
      if (in_graph == null)
         return false;
      
      verts_in_graph = in_graph.getVertSet();
      for (vert_iter = verts_in_graph.iterator(); vert_iter.hasNext(); )
      {
         src =  vert_iter.next();
         for (edge_iter = src.adj_list.iterator(); edge_iter.hasNext(); )
         {
            edge = edge_iter.next();
            dst = edge.first;
            cost = edge.second;
            edge_heap.add( new FHedge<E>(src, dst, cost) );
         }
      }
      return true;
   }
}
