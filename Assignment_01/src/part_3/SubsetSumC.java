package part_3;

//---------------------------------------------------------------------------
//CIS 27C Assignment #1 Part B - iTunes Version

import cs_1c.*;

import java.text.*;
import java.util.*;

/**
 * Test class SubsetSumB to test class Sublist
 * 
 * @author Zhongtian Wang
 * @version April 17, 2013
 */
public class SubsetSumC
{
   public static void main(String[] args) throws Exception
   {
      int TARGET = 3600;
      ArrayList<iTunesEntry> data_set = new ArrayList<iTunesEntry>();
      ArrayList<Sublist<iTunesEntry>> choices = new ArrayList<Sublist<iTunesEntry>>();
      int k, j, current_size, array_size, max_index = 0;
      boolean found_perfect = false;

      // for formatting and timing
      NumberFormat tidy = NumberFormat.getInstance(Locale.US);
      tidy.setMaximumFractionDigits(4);
      long start_time, end_time;

      // read the iTunes Data
      iTunesEntryReader tunes_input = new iTunesEntryReader("itunes_file.txt");

      // test the success of the read:
      if (tunes_input.readError())
      {
         System.out.println("couldn't open " + tunes_input.getFileName()
               + " for input.");
         return;
      }

      // load the data_set ArrayList with the iTunes:
      array_size = tunes_input.getNumTunes();
      for (k = 0; k < array_size; k++)
         data_set.add(tunes_input.getTune(k));

      choices.clear();
      System.out.println("Target time: " + TARGET);

      // START TIMING THE ALGORITHM
      start_time = System.nanoTime();
      choices.add(new Sublist(data_set)); // this represents the 0 set

      for (int i = 0; i < data_set.size(); i++)
      {
         current_size = choices.size();
         for (j = 0; j < current_size; j++)
         {
            int sum = choices.get(j).getSum() + data_set.get(i).getNTime();
            if (sum < TARGET)
            {
               choices.add(choices.get(j).addItem(i, data_set.get(i).getNTime()));
            } else if (sum == TARGET)
            {
               choices.add(choices.get(j).addItem(i, data_set.get(i).getNTime()));
               found_perfect = true;
               max_index = choices.size() - 1;
               break;
            }
         }
         if (found_perfect)
         {
            break;
         }
      }

      end_time = System.nanoTime();

      choices.get(max_index).showSublist();

      // report algorithm time
      System.out.println("\nAlgorithm Elapsed Time: "
            + tidy.format((end_time - start_time) / 1e9) + " seconds.");
   }

   /**
    * Class Sublist
    * 
    * @author Zhongtian Wang
    * @version April 17, 2013
    */
   private static class Sublist<E> implements Cloneable
   {
      private int sum = 0;
      private ArrayList<E> original_objects;
      private ArrayList<Integer> indices;

      /**
       * Default constructor that constructs an empty Generic object list
       */
      public Sublist(ArrayList<E> data_set)
      {
         sum = 0;
         original_objects = data_set;
         indices = new ArrayList<Integer>();
      }

      /**
       * Returns sum
       * 
       * @return sum
       */
      int getSum()
      {
         return sum;
      }

      /**
       * Clones this Generic object
       * 
       * @throws CloneNotSupportedException
       * @return the cloned object "new_object"
       */
      public Object clone() throws CloneNotSupportedException
      {
         Sublist<E> new_object = (Sublist<E>) super.clone();
         new_object.indices = (ArrayList<Integer>) indices.clone();

         return new_object;
      }

      /**
       * Returns a new Generic object with one reference added in the end
       * 
       * @param index
       * @return newList the new Generic object
       */
      Sublist<E> addItem(int index, int value)
      {
         Sublist<E> newList = new Sublist<E>(original_objects);
         try
         {
            newList = (Sublist<E>) this.clone();
         } catch (CloneNotSupportedException e)
         {
            System.out.println(e);
         }
         newList.sum += value;
         newList.indices.add(index);
         return newList;
      }

      /**
       * Prints the Generic object to the console
       */
      void showSublist()
      {
         System.out.println("Sublist========================================="
               + "\nSum = " + sum);
         for (int i = 0; i < indices.size(); i++)
            System.out.println("array[" + indices.get(i) + "] = "
                  + original_objects.get(indices.get(i)));
      }
   }
}

/*-----------------------------OUTPUT-------------------------------
 Target time: 3600
 Sublist=========================================
 Sum = 3600
 array[0] = Carrie Underwood | Cowboy Casanova |  3:56
 array[1] = Carrie Underwood | Quitter |  3:40
 array[2] = Rihanna | Russian Roulette |  3:48
 array[4] = Foo Fighters | Monkey Wrench |  3:50
 array[5] = Eric Clapton | Pretending |  4:43
 array[6] = Eric Clapton | Bad Love |  5:08
 array[7] = Howlin' Wolf | Everybody's In The Mood |  2:58
 array[8] = Howlin' Wolf | Well That's All Right |  2:55
 array[9] = Reverend Gary Davis | Samson and Delilah |  3:36
 array[11] = Roy Buchanan | Hot Cha |  3:28
 array[12] = Roy Buchanan | Green Onions |  7:23
 array[13] = Janiva Magness | I'm Just a Prisoner |  3:50
 array[14] = Janiva Magness | You Were Never Mine |  4:36
 array[15] = John Lee Hooker | Hobo Blues |  3:07
 array[16] = John Lee Hooker | I Can't Quit You Baby |  3:02

 Algorithm Elapsed Time: 0.1171 seconds.
 ---------------------------------------------------------------------*/