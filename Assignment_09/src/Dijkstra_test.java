//FHgraph and djikstra algorithm
//CS 1C Foothill College
import cs_1c.*;
import java.util.*;
import java.text.*;

//------------------------------------------------------
public class Dijkstra_test
{
   // ------- main --------------
   public static void main(String[] args) throws Exception
   {
      // build graph
      FHgraph<String> my_graph_1 = new FHgraph<String>();
      my_graph_1.addEdge("v1", "v2", 2);
      my_graph_1.addEdge("v1", "v4", 1);
      my_graph_1.addEdge("v2", "v4", 3);
      my_graph_1.addEdge("v2", "v5", 10);
      my_graph_1.addEdge("v3", "v1", 4);
      my_graph_1.addEdge("v3", "v6", 5);
      my_graph_1.addEdge("v4", "v3", 2);
      my_graph_1.addEdge("v4", "v5", 2);
      my_graph_1.addEdge("v4", "v6", 8);
      my_graph_1.addEdge("v4", "v7", 4);
      my_graph_1.addEdge("v5", "v7", 6);
      my_graph_1.addEdge("v7", "v6", 1);

      my_graph_1.showAdjTable();

      // dijkstra called from inside
      my_graph_1.showDistancesTo("v2");
      System.out.println();

      my_graph_1.showShortestPath("v2", "v3");
      //my_graph_1.showShortestPath("v2", "v6");
      //my_graph_1.showShortestPath("v2", "v7");
   }
}