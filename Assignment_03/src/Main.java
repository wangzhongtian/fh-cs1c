import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;


public class Main
{
   final static int MAT_SIZE = 800;

   public static void main(String[] args) throws Exception
   {
      int r, rand_row, rand_col;
      long start_time, end_time;
      double rand_frac;
      double small_percent;
      NumberFormat tidy = NumberFormat.getInstance(Locale.US);
      tidy.setMaximumFractionDigits(4);

      double[][] mat, mat_ans;
      mat = new double[MAT_SIZE][MAT_SIZE];
      mat_ans = new double[MAT_SIZE][MAT_SIZE];

      // generate small% of non-default values bet 0 and 1
      small_percent = MAT_SIZE / 20. * MAT_SIZE;
      for (r = 0; r < small_percent; r++)
      {
         Random generator = new Random();
         rand_row = generator.nextInt(MAT_SIZE);
         rand_col = generator.nextInt(MAT_SIZE);
         rand_frac = generator.nextDouble();
         mat[rand_row][rand_col] = rand_frac;
      }

      // 10x10 submatrix in lower right
      System.out.println("Original");
      MatrixMultiply.matrixShow(mat, MAT_SIZE - 10, 10);
      start_time = System.nanoTime();
      MatrixMultiply.matrixMultiply(mat, mat, mat_ans);

      end_time = System.nanoTime();

      System.out.println("\nAfter Multiply");
      MatrixMultiply.matrixShow(mat_ans, MAT_SIZE - 10, 10);

      System.out.println("\nSize = " + MAT_SIZE + " Mat. Mult. Elapsed Time: "
            + tidy.format((end_time - start_time) / 1e9) + " seconds.");
   }
}
