import cs_1c.*;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * The spare matrix class SparseMat
 * 
 * @author Zhongtian Wang
 * @param <E>
 *           generic type E
 * @version 04/24/2013
 */
public class SparseMat<E> implements Cloneable
{
   protected int row_size, col_size;
   protected E default_val;
   protected FHarrayList<FHlinkedList<MatNode>> rows;

   /**
    * Node class MatNode
    * 
    * @author Zhongtian Wang
    * @version 04/24/2013
    */
   protected class MatNode
   {
      public int col;
      public E data;

      /**
       * Default constructor
       */
      MatNode()
      {
         col = 0;
         data = null;
      }

      /**
       * Parameterized constructor
       * 
       * @param cl
       *           column number
       * @param dt
       *           data value
       */
      MatNode(int cl, E dt)
      {
         col = cl;
         data = dt;
      }

      /**
       * Returns the column of this Node
       * 
       * @return
       */
      public int getCol()
      {
         return col;
      }
   };

   /**
    * Parameterized constructor
    * 
    * @param num_rows
    *           rows size
    * @param num_cols
    *           columns size
    * @param default_v
    *           default value
    */
   public SparseMat(int num_rows, int num_cols, E default_v)
   {
      if (num_rows < 1 || num_cols < 1)
      {
         throw new IndexOutOfBoundsException();
      }
      row_size = num_rows;
      col_size = num_cols;
      // rows.ensureCapacity(num_rows);
      rows = new FHarrayList<FHlinkedList<MatNode>>(num_rows);
      default_val = default_v;

      allocateEmptyMatrix();
   }

   /**
    * Allocate all the memory of this matrix
    */
   private void allocateEmptyMatrix()
   {
      for (int i = 0; i < col_size; i++)
      {
         FHlinkedList<MatNode> init_list = new FHlinkedList<MatNode>();
         rows.add(init_list);
      }
      clear();
   }

   /**
    * Returns the data stored in the position [r][c] of the SparseMat
    * 
    * @param r
    *           row number
    * @param c
    *           column number
    * @return the data stored in the position [r][c] of the SparseMat
    */
   E get(int r, int c)
   {
      if ((r < 0 || r >= row_size) || (c < 0 || c >= col_size))
      {
         throw new IndexOutOfBoundsException();
      }

      for (int i = 0; i < rows.get(r).size(); i++)
      {
         if (rows.get(r).get(i).getCol() == c)
            return rows.get(r).get(i).data;
      }
      return default_val;
   }

   /**
    * Sets the value of the data in the position [r][c] of the SparseMat with x
    * 
    * @param r
    *           row number
    * @param c
    *           column number
    * @param x
    *           the data
    * @return true if it sets the value successfully of false otherwise
    */
   boolean set(int r, int c, E x)
   {
      if ((r < 0 || r >= row_size) || (c < 0 || c >= col_size))
      {
         return false;
      }
      // if data exists
      for (int i = 0; i < rows.get(r).size(); i++)
      {
         if (rows.get(r).get(i).getCol() == c)
         {
            if (x != default_val)
            {
               rows.get(r).get(i).data = x;
               return true;
            } else
            {
               rows.get(r).remove(i);
               return true;
            }

         }
      }

      // If data does not exist
      if (x != default_val)
      {
         MatNode node = new MatNode(c, x);
         rows.get(r).add(node);
         return true;
      }
      return false;
   }

   /**
    * Copies the value from one location (rSrc, cSrc) to another location (rDst,
    * cDst)
    * 
    * @param rSrc
    *           the source row
    * @param cSrc
    *           the source column
    * @param rDst
    *           the destination row
    * @param cDst
    *           the destination column
    * @return true if the value copied successfully of false otherwise
    */
   boolean copyValue(int rSrc, int cSrc, int rDst, int cDst)
   {
      if ((rSrc < 0 || rSrc >= row_size) || (rDst < 0 || rDst >= row_size)
            || (cSrc < 0 || cSrc >= col_size) || (cDst < 0 || cDst >= col_size))
      {
         return false;
      }

      this.set(rDst, cDst, this.get(rSrc, cSrc));
      return true;
   }

   /**
    * Clears all the rows, effectively setting all values to the default_val
    */
   void clear()
   {
      for (int i = 0; i < row_size; i++)
      {
         rows.get(i).clear();
      }
   }

   /**
    * Returns a new cloned sparse matrix
    */
   public Object clone()
   {
      SparseMat<E> clone = new SparseMat<E>(row_size, col_size, default_val);
      for (int i = 0; i < row_size; i++)
      {
         for (int j = 0; j < rows.get(i).size(); j++)
         {
            clone.set(i, rows.get(i).get(j).getCol(), rows.get(i).get(j).data);
         }
      }
      return clone;
   }

   /**
    * Shows a square sub-matrix anchored at (start, start) and whose size is
    * size x size
    * 
    * @param start
    *           the start row and column number
    * @param size
    *           the width and length of the squared display
    */
   void showSubSquare(int start, int size)
   {
      int stop = start + size - 1;
      for (int i = start; i <= stop; i++)
      {
         for (int j = start; j <= stop; j++)
         {
            Object data = get(i, j);
            System.out.printf("%5s", data);
         }
         System.out.println();
      }
      System.out.println();
   }

   /**
    * Main method for testing
    */
   public static void main(String[] args) throws Exception
   {
      NumberFormat tidy = NumberFormat.getInstance(Locale.US);
      tidy.setMaximumFractionDigits(4);
      long start_time, end_time;
      start_time = System.nanoTime();

      test();

      end_time = System.nanoTime();
      System.out.println("\nTotal Testing Time: "
            + tidy.format((end_time - start_time) / 1e9) + " seconds.");
   }

   /**
    * Main testing class
    */
   private static void test()
   {
      final int MAT_SIZE = 100000;
      System.out.println("Initializing a sparse matrix with size " + MAT_SIZE
            + "*" + MAT_SIZE + "...\n");
      SparseMat mat = new SparseMat(MAT_SIZE, MAT_SIZE, 0.);

      System.out.println("-----Testing setter and getter-----");
      System.out
            .println("--Adding 10.0 to [0][0] and adding 11.0 [99999][99999]");
      mat.set(0, 0, 10.);
      mat.set(MAT_SIZE - 1, MAT_SIZE - 1, 11.0);
      System.out.println("--Return the new value");
      System.out.println("  [0][0] now has value " + mat.get(0, 0)
            + ". [99999][99999] has value "
            + mat.get(MAT_SIZE - 1, MAT_SIZE - 1));

      System.out.println("--Overwriting value in [0][0] with value 19.0");
      mat.set(0, 0, 19.);
      System.out.println("  [0][0] now has value " + mat.get(0, 0));

      System.out.println("--Overwriting value in [0][0]"
            + " with default value 0.0");
      mat.set(0, 0, 0.);
      System.out.println("  [0][0] now has value " + mat.get(0, 0));

      System.out.println("--Adding value to illegal position [100000][1]"
            + "--should fail silently");
      if (mat.set(100000, 1, 6.))
      {
         System.out.println("  Return false failed");
      } else
      {
         System.out.println("  Return false succeeded");
      }

      System.out.println("--Getting value from illegal position [-1][1]"
            + "--should throw exception");
      try
      {
         mat.get(-1, 1);
         System.out.println("  Catch exception failed");
      } catch (IndexOutOfBoundsException e)
      {
         System.out.println("  Catch exception succeeded");
      }

      System.out.println("\n-----Testing the copier-----");
      System.out.println("--Set value in [4][4] as 15.0 and copy it to"
            + " [5][5] using copyValue");
      mat.set(4, 4, 15.);
      mat.copyValue(4, 4, 5, 5);
      System.out.println("  [4][4] now has value " + mat.get(4, 4)
            + ", and [5][5] now has value " + mat.get(5, 5));

      System.out.println("--Copy value from [10][10] (Default Value) "
            + "to [5][5] using copyValue");
      mat.copyValue(10, 10, 5, 5);
      System.out.println("  [5][5] now has value " + mat.get(5, 5));

      System.out.println("--Copy value from illegal position [100000][10] "
            + "to [5][5] using copyValue");
      if (mat.copyValue(100000, 10, 5, 5))
      {
         System.out.println("  Return false failed");
      } else
      {
         System.out.println("  Return false succeeded");
      }

      System.out.println("\n-----Testing to show the matrices-----");
      System.out.println("--Setting the [0][0] with value 99.0 "
            + "and show top left 15x15");
      mat.set(0, 0, 99.);
      mat.showSubSquare(0, 15);

      System.out.println("--Setting the [0][0] with value 99.0 "
            + "and show bottom right 15x15");
      mat.set(MAT_SIZE - 1, MAT_SIZE - 1, 99.);
      mat.showSubSquare(MAT_SIZE - 15, 15);

      System.out.println("\n-----Testing clone() method-----");
      SparseMat mat_clone = (SparseMat) mat.clone();
      System.out.println("--Show top left 15x15 of the cloned matrix");
      mat_clone.showSubSquare(0, 15);
      System.out.println("--Show bottom right 15x15 of the cloned matrix");
      mat_clone.showSubSquare(MAT_SIZE - 15, 15);

      System.out.println("--Checking if the cloned matrix is constructed "
            + "by deep copy, so remove [0][0] in the original matrix");
      mat.set(0, 0, 0.);
      System.out.println("  [0][0] in the new matrix has value "
            + mat_clone.get(0, 0));

      System.out.println("\n-----Testing clear() method-----");
      System.out.println("--Cloned list calling clear() method");
      mat_clone.clear();
      System.out.println("--Show top left 15x15 of the cloned matrix");
      mat_clone.showSubSquare(0, 15);
      System.out.println("--Show bottom right 15x15 of the cloned matrix");
      mat_clone.showSubSquare(MAT_SIZE - 15, 15);

      System.out.println("\nTesting completed.");
   }
}


/*---------------------------------OUTPUT---------------------------------------

Initializing a sparse matrix with size 100000*100000...

-----Testing setter and getter-----
--Adding 10.0 to [0][0] and adding 11.0 [99999][99999]
--Return the new value
  [0][0] now has value 10.0. [99999][99999] has value 11.0
--Overwriting value in [0][0] with value 19.0
  [0][0] now has value 19.0
--Overwriting value in [0][0] with default value 0.0
  [0][0] now has value 0.0
--Adding value to illegal position [100000][1]--should fail silently
  Return false succeeded
--Getting value from illegal position [-1][1]--should throw exception
  Catch exception succeeded

-----Testing the copier-----
--Set value in [4][4] as 15.0 and copy it to [5][5] using copyValue
  [4][4] now has value 15.0, and [5][5] now has value 15.0
--Copy value from [10][10] (Default Value) to [5][5] using copyValue
  [5][5] now has value 0.0
--Copy value from illegal position [100000][10] to [5][5] using copyValue
  Return false succeeded

-----Testing to show the matrices-----
--Setting the [0][0] with value 99.0 and show top left 15x15
 99.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0 15.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0

--Setting the [0][0] with value 99.0 and show bottom right 15x15
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 99.0


-----Testing clone() method-----
--Show top left 15x15 of the cloned matrix
 99.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0 15.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0

--Show bottom right 15x15 of the cloned matrix
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0 99.0

--Checking if the cloned matrix is constructed by deep copy, so remove [0][0] in
 the original matrix
  [0][0] in the new matrix has value 99.0

-----Testing clear() method-----
--Cloned list calling clear() method
--Show top left 15x15 of the cloned matrix
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0

--Show bottom right 15x15 of the cloned matrix
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0
  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0  0.0


Testing completed.

Total Testing Time: 0.2257 seconds.

-----------------------------------------------------------------------------*/