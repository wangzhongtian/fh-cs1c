public class MatrixMultiply
{
   public static void matrixMultiply(double[][] mat_a, double[][] mat_b,
         double[][] mat_answer)
   {
      if (mat_a[0].length != mat_b.length)
         throw new IndexOutOfBoundsException();
      
      double[][] trans_mat;
      trans_mat = new double[mat_b.length][mat_b[0].length];
      
      matrixTranspose(mat_b, trans_mat);
      double temp = 0.0;
      
      for (int r = 0; r < mat_a.length; r++)
      {
         for (int c = 0; c < trans_mat.length; c++)
         {
            for (int k = 0; k < trans_mat[0].length; k++)
            {
               temp += mat_a[r][k] * trans_mat[c][k];
            }
            mat_answer[r][c] = temp;
            temp = 0.0;
         }
      }
   }

   public static void matrixTranspose(double[][] mat_orig, double[][] mat_trans)
   {
      for (int r = 0; r < mat_orig.length; r++)
      {
         for (int c = 0; c < mat_orig[0].length; c++)
         {
            mat_trans[c][r] = 0.0;
            mat_trans[c][r] = mat_orig[r][c];
         }
      }
   }
   
   public static void matrixShow(double[][] mat_a, int start, int size)
   {
      System.out.println("row length: " + mat_a[0].length + " col length: " 
            + mat_a.length + " start: " + start + " size: " + size);
      if ((start > mat_a.length || start > mat_a[0].length)
            || (size > mat_a.length - start || size > mat_a[0].length - start))
      {
         throw new IndexOutOfBoundsException();
      }
      for (int r = start; r < start + size; r++)
      {
         for (int c = start; c < start + size; c++)
         {
            System.out.printf("%.2f ", mat_a[r][c]);
         }
         System.out.println();
      }
   }
}

/* ===============================Questions====================================
Estimation: T(N) = O(N^3)---there're three "for" loops together, makes it N^3
            
            T(N) = Θ(N^3)---It's the same because the worst case would happen 
            every time we multiply. There is no way to jump out of the loop

Questions:  1. For data size 8 * 8 is the smallest
            2.    Data Size     Runtime
                  100           0.03
                  200           0.05
                  400           0.13
                  800           0.91
                  1600          6.8
                  3200          50.1
                  6400          437.3
            3. When data size is 6400, it starts to take a long time to run,
               which is 437 seconds.
            4. Agrees really well! Plotting values from 2 in Excel and drawing 
               graph with adding a cubic fit, we can confirm that Θ(N^3).
*/


/*====================================Run #1===================================
Original
row length: 400 col length: 400 start: 390 size: 10
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.08 
0.00 0.00 0.00 0.00 0.01 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.00 0.00 0.00 0.59 0.00 0.00 0.00 0.00 
0.00 0.00 0.90 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.47 0.00 0.00 0.00 0.00 0.00 0.00 0.68 0.00 
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.21 0.00 
0.00 0.00 0.00 0.55 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 

After Multiply
row length: 400 col length: 400 start: 390 size: 10
0.00 0.20 0.14 0.00 0.25 0.00 0.11 0.00 0.74 0.19 
0.00 0.00 0.03 0.40 0.62 0.00 0.12 0.08 0.00 0.00 
0.00 0.00 0.04 0.36 0.00 1.02 0.00 0.00 0.00 0.00 
0.00 0.27 0.00 0.42 0.00 0.21 0.06 0.00 0.68 0.00 
0.00 0.32 0.10 0.55 0.71 0.29 0.26 0.27 0.82 0.00 
0.00 0.46 0.32 0.60 0.30 0.29 0.05 0.00 0.58 0.00 
0.00 0.38 0.00 0.44 0.83 0.00 0.00 0.41 0.00 0.57 
0.00 0.12 0.47 0.70 0.00 0.09 0.00 0.35 0.76 0.46 
0.00 0.03 0.00 0.00 0.00 1.16 0.08 0.00 0.14 0.78 
0.00 0.00 0.33 0.10 0.35 0.34 0.19 0.29 0.00 1.08 

Size = 400 Mat. Mult. Elapsed Time: 0.1271 seconds.
*/


/*====================================Run #2===================================
Original
row length: 800 col length: 800 start: 790 size: 10
0.00 0.85 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.71 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 

After Multiply
row length: 800 col length: 800 start: 790 size: 10
0.34 0.26 0.00 0.09 0.42 0.28 0.00 0.66 0.31 0.46 
0.18 0.01 0.30 0.29 0.00 0.00 0.38 0.66 0.00 0.76 
0.29 0.58 0.10 0.00 0.26 0.57 0.32 0.54 0.23 1.02 
1.05 0.13 0.10 1.14 0.57 0.49 0.05 0.24 0.00 0.19 
0.22 0.12 0.80 1.46 1.10 0.36 0.08 0.11 0.00 0.02 
0.00 0.57 0.24 0.32 0.55 0.06 0.04 1.11 1.07 0.00 
0.16 0.19 0.65 0.78 1.50 0.51 0.32 0.17 0.14 0.28 
0.37 0.59 0.36 0.00 0.15 0.22 0.16 0.48 0.71 0.16 
0.15 1.05 0.14 0.33 0.00 0.05 0.13 0.72 0.10 0.27 
0.19 0.00 0.00 0.56 1.01 1.45 0.65 0.60 0.11 0.15 

Size = 800 Mat. Mult. Elapsed Time: 1.0683 seconds.
*/