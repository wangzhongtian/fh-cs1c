import cs_1c.EBookEntry;

class EBookCompString implements Comparable<String>
{
   EBookEntry data;

   public EBookCompString(EBookEntry e)
   {
      data = e;
   }

   public String toString()
   {
      return data.toString();
   }

   // we'll use compareTo() to implement our find on key
   public int compareTo(String key)
   {
      return data.getSTitle().compareTo(key);
   }

   // let equals() preserve the equals() provided by embedded data
   public boolean equals(EBookCompString rhs)
   {
      return data.equals(rhs.data);
   }

   public int hashCode()
   {
      return data.getSTitle().hashCode();
   }
}