import cs_1c.FHhashQP;

public class HashQPwFind<KeyType, E extends Comparable<KeyType>> extends
      FHhashQP<E>
{
   public HashQPwFind()
   {
      super();
   }

   public HashQPwFind(int table_size)
   {
      super(table_size);
   }

   public boolean insert(KeyType key, E x)
   {

      int bucket = findPosKey(key);
      if (m_array[bucket].state == ACTIVE)
         return false;
      m_array[bucket].data = x;
      m_array[bucket].state = ACTIVE;
      if (++m_size > m_max_lambda * m_table_size)
         rehash();
      return true;
   }

   public E find(KeyType key)
   {
      int index = findPosKey(key);
      if (m_array[index].state != ACTIVE)
      {
         throw new java.util.NoSuchElementException();
      }
      return m_array[index].data;
   }

   protected int findPosKey(KeyType key)
   {
      int kth_odd_num = 1;
      int index = myhashKey(key);

      while (m_array[index].state != EMPTY
            && m_array[index].data.compareTo(key) != 0)
      {
         index += kth_odd_num;
         kth_odd_num += 2;
         if (index >= m_table_size)
            index -= m_table_size;
      }
      return index;
   }

   protected int myhashKey(KeyType key)
   {
      int hash_key;
      hash_key = key.hashCode() % m_table_size;
      if (hash_key < 0)
         hash_key += m_table_size;
      return hash_key;
   }
}