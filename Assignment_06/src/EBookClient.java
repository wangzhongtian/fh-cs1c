import java.util.*;

import cs_1c.*;

public class EBookClient
{
   public static final int NUM_RANDOM_INDICES = 25;
   
   public static void main(String[] args) throws Exception
   {
      EBookEntryReader reader = new EBookEntryReader("catalog-short4.txt");
      if (reader.readError())
         System.out.println("Failed opening: " + reader.getFileName());
      int array_size = reader.getNumBooks();

      HashQPwFind<String, EBookCompString> hash_table = new HashQPwFind<String, EBookCompString>(
            array_size);
      HashQPwFind<Integer, EBookCompInt> hash_table_int = new HashQPwFind<Integer, EBookCompInt>(
            array_size);

      EBookCompString[] s_array = new EBookCompString[array_size];
      EBookCompInt[] i_array = new EBookCompInt[array_size];

      for (int k = 0; k < array_size; k++)
      {
         s_array[k] = new EBookCompString(reader.getBook(k));
         i_array[k] = new EBookCompInt(reader.getBook(k));
      }

      for (int i = 0; i < array_size; i++)
      {
         hash_table.insert(s_array[i].data.getSTitle(), s_array[i]);
         hash_table_int.insert(i_array[i].data.getNEtextNum(), i_array[i]);
      }

      Random random = new Random();
      System.out.println("The same random books from the hash table ");
      for (int i = 0; i < NUM_RANDOM_INDICES; i++)
      {
         int int_random = random.nextInt(array_size);
         try
         {
            System.out.println("=====TESTING EBookCompString=====");
            System.out.println("Finding: "
                  + s_array[int_random].data.getSTitle());
            EBookCompString book_result = hash_table
                  .find(s_array[int_random].data.getSTitle());
            System.out.println("Found: " + book_result.data.getSTitle());

            if (book_result.data.getSTitle() != s_array[int_random].data
                  .getSTitle())
            {
               System.out.println("Book result doesn't match.");
            }

            System.out.println("\n=====TESTING EBookCompInt=====");
            System.out.println("Finding: "
                  + i_array[int_random].data.getNEtextNum());
            EBookCompInt book_result_int = hash_table_int
                  .find(i_array[int_random].data.getNEtextNum());
            System.out.println("Found: "
                  + i_array[int_random].data.getNEtextNum());

            if (book_result_int.data.getNEtextNum() != i_array[int_random].data
                  .getNEtextNum())
            {
               System.out.println("Book result doesn't match.");
            }
         } catch (NoSuchElementException e)
         {
            System.out.println("Unexpected exception: " + e);
         }
         System.out.println();
      }

      System.out.println("Testing know exceptions.....");

      // test known failures exceptions:
      try
      {
         System.out.print("Test 1 (entry not exist): ");
         EBookCompString str_search_result = hash_table.find("11221");
         System.out.println( "Found " + str_search_result );
      } catch (NoSuchElementException e)
      {
         System.out.println("Catch exception succeeded");
      }

      try
      {
         System.out.print("Test 2 (index not exist): ");
         EBookCompInt int_search_result = hash_table_int.find(-1);
         System.out.println( "Found " + int_search_result );
      } catch (NoSuchElementException e)
      {
         System.out.println("Catch exception succeeded");
      }

      try
      {
         System.out.print("Test 3 (index too big): ");
         EBookCompInt int_search_result = hash_table_int.find(array_size + 1);
         System.out.println( "Found " + int_search_result );
      } catch (NoSuchElementException e)
      {
         System.out.println("Catch exception succeeded");
      }
   }
}
