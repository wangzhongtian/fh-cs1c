import cs_1c.EBookEntry;

class EBookCompInt implements Comparable<Integer>
{
   EBookEntry data;

   public EBookCompInt(EBookEntry e)
   {
      data = e;
   }

   public String toString()
   {
      return data.toString();
   }

   // we'll use compareTo() to implement our find on key
   public int compareTo(Integer key)
   {
      return data.getNEtextNum() - key;
   }

   // let equals() preserve the equals() provided by embedded data
   public boolean equals(EBookCompInt rhs)
   {
      return data.equals(rhs.data);
   }

   public int hashCode()
   {
      return data.getNEtextNum();
   }
}